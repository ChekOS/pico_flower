#include <Arduino.h>
#include <WiFi.h>
#include <ESP32Servo.h>
#include <stream.h> 
#include <iostream> 
#include <string>

Servo myservo;  // create servo object to control a servo

// Servo GPIO pin
static const int servoPin = 32;
int angle;
int angle_min = 0;
int angle_max = 180;
int angle_change = 15;
const int measurement_num = 13; // (angle_max - angle_min) / angle_change + 1;
int measurements[measurement_num];
int degrees[measurement_num];
#define LIGHT_SENSOR_PIN 36

// Network credentials
WiFiClient client;
const char* ssid     = "Redmi Note 9S";
const char* password = "1234567890";

// Web server on port 80 (http)
WiFiServer server(80);

// Variable to store the HTTP request
String header;

// Decode HTTP GET value8
String valueString = String(5);
int pos1 = 0;
int pos2 = 0;

// Current time
unsigned long currentTime = millis();
// Previous time
unsigned long previousTime = 0; 
// Define timeout time in milliseconds (example: 2000ms = 2s)
const long timeoutTime = 2000;

int maximum(int arr[], int n)
{
    int i;
      
    // Initialize maximum element
    int max = arr[0];
  
    // Traverse array elements 
    // from second and compare
    // every element with current max 
    for (i = 1; i < n; i++)
        if (arr[i] > max)
            max = arr[i];
  
    return max;
}

int indexof(int arr[], int x, int arrLen) {
     
    int index = -1;
    Serial.println(x);
    Serial.println(arrLen); 
    for (int i = 0; i < arrLen; i++) {
        Serial.println(arrLen);
        Serial.println(arr[i]);
        if (arr[i] == x) {
            index = i;
            break;
        }
    }
    Serial.println(index); 
    if (index > -1) {
        return index;
    } else {
       return 0;
    }
         
}

int autoMode()
{
    myservo.write(angle_min);

    int counter = 0;
    for (angle = angle_min; angle <= angle_max; angle += angle_change) { 
 
        myservo.write(angle);
        delay(500);
        int analogValue = analogRead(LIGHT_SENSOR_PIN);
        measurements[counter] = analogValue;
        degrees[counter] = angle;
        counter++;
        Serial.print("Angle: ");
        Serial.print(angle);
        Serial.print(" Value: ");
        Serial.println(analogValue);
        delay(500);
    }

    int n = sizeof(measurements) / sizeof(measurements[0]);

    int measurement_largest = maximum(measurements, n);
    int angle_largest = indexof(measurements, measurement_largest, n) * angle_change;

    myservo.write(angle_largest);

    Serial.print("Max Angle: ");
    Serial.print(angle_largest);
    Serial.print(" Max Value: ");
    Serial.println(measurement_largest);

    // Chart
    client.println("<script>var chartVariables = [");
    for (int i = 0; i < measurement_num; i++)
      client.printf("{x:%d, y:%d}", degrees[i], measurements[i]);
    client.println("]; console.log(chartVariables);</script>");

    return 0;
}

void setup() {
  
  // Allow allocation of all timers for servo library
  ESP32PWM::allocateTimer(0);
  ESP32PWM::allocateTimer(1);
  ESP32PWM::allocateTimer(2);
  ESP32PWM::allocateTimer(3);
  
  // Set servo PWM frequency to 50Hz
  myservo.setPeriodHertz(50);
  
  // Attach to servo and define minimum and maximum positions
  // Modify as required
  myservo.attach(servoPin, 500, 2400);
  
  // Start serial
  Serial.begin(115200);

  pinMode(18, OUTPUT);

  // Connect to Wi-Fi network with SSID and password
  Serial.print("Connecting to: ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  // Print local IP address and start web server
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  server.begin();
}

bool isConnected = false;

void loop(){
  
  if (WiFi.status() == WL_CONNECTED && !isConnected) {
    Serial.println("Connected");
    digitalWrite(18, HIGH);
    isConnected = true;
  }

  // Listen for incoming clients
  WiFiClient client = server.available();   
  
  // Client Connected
  if (client) {                             
    // Set timer references
    currentTime = millis();
    previousTime = currentTime;
    
    // Print to serial port
    Serial.println("New Client."); 
    
    // String to hold data from client
    String currentLine = ""; 
    
    // Do while client is connected
    while (client.connected() && currentTime - previousTime <= timeoutTime) { 
      currentTime = millis();
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        header += c;
        if (c == '\n') {                    // if the byte is a newline character
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
        
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK) and a content-type
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();

            // Display the HTML web page
            
            // HTML Header
            client.println("<!DOCTYPE html><html>");
            client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            client.println("<link rel=\"icon\" href=\"data:,\">");
            
            // CSS - Modify as desired
            client.println("<style>body { text-align: center; font-family: \"Trebuchet MS\", Arial; margin-left:auto; margin-right:auto; }");
            client.println(".slider { -webkit-appearance: none; width: 300px; height: 25px; border-radius: 10px; background: #ffffff; outline: none;  opacity: 0.7;-webkit-transition: .2s;  transition: opacity .2s;}");
            client.println(".slider::-webkit-slider-thumb {-webkit-appearance: none; appearance: none; width: 35px; height: 35px; border-radius: 50%; background: #ff3410; cursor: pointer; }");
            
            client.println("input[type=\"checkbox\" i] {height: 25px; width: 25px;};");
            client.println("input[type=\"checkbox\"]#modeCheckbox:checked + span {border-color: red; background-color:red;}");
            client.println("</style>");
            // Get JQuery
            client.println("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>");
            client.println("<script src=\"https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js\"></script>");
                     
            // Page title
            client.println("</head><body style=\"background-color:#F8C456;\"><h1 style=\"color:#F4802B;\">Servo Control</h1>");
            
            // Position display
            client.println("<h2 style=\"color:#ffffff;\">Position: <span id=\"servoPos\"></span>&#176;</h2>"); 
                     
            // Slider control
            client.println("<input type=\"range\" min=\"0\" max=\"180\" class=\"slider\" id=\"servoSlider\" onchange=\"servo(this.value)\" value=\""+valueString+"\"/><br>");
            
            // Slider Javascript
            client.println("<script>var slider = document.getElementById(\"servoSlider\");");
            client.println("var servoP = document.getElementById(\"servoPos\"); servoP.innerHTML = slider.value;");
            client.println("slider.oninput = function() { slider.value = this.value; servoP.innerHTML = this.value; }");
            client.println("$.ajaxSetup({timeout:1000}); function servo(pos) { ");
            client.println("$.get(\"/?value=\" + pos + \"&\"); {Connection: close};}</script>");

            // Mode control
            client.println("<h3 style=\"color:#ffffff;\"><input type=\"checkbox\" class=\"checkbox\" id=\"modeCheckbox\" onchange=\"modeChange(this.checked)\"/>");
            client.println("<label for=\"modeCheckbox\">Automated mode</label></h3>");
            client.println("<canvas id=\"myChart\" style=\"width:100%;max-width:700px\"></canvas>");
            
            // Mode Javascript
            client.println("<script>var mode = document.getElementById(\"modeCheckbox\");");
            client.println("$.ajaxSetup({timeout:1000}); function modeChange(mode) { ");
            client.println("$.get(\"/?mode=\" + mode + \"&\"); {Connection: close};}</script>");
            
            // End page
            client.println("</body></html>");     
            
            // GET Slider data
            if(header.indexOf("GET /?value=")>=0) {
              pos1 = header.indexOf('=');
              pos2 = header.indexOf('&');
              
              // String with motor position
              valueString = header.substring(pos1+1, pos2);
              
              // Move servo into position
              myservo.write(valueString.toInt());
              
              // Print value to serial monitor
              Serial.print("Val = ");
              Serial.println(valueString); 
            }

            // GET Slider data
            if(header.indexOf("GET /?mode=")>=0) {
              pos1 = header.indexOf('=');
              pos2 = header.indexOf('&');
              
              // String with motor position
              valueString = header.substring(pos1+1, pos2);
              
              // Print value to serial monitor
              Serial.print("Mode = ");
              Serial.println(valueString); 

              if (valueString == "true") {
                autoMode();
              }
            } 

            // The HTTP response ends with another blank line
            client.println();
            
            // Break out of the while loop
            break;
          
          } else { 
            // New lline is received, clear currentLine
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    // Clear the header variable
    header = "";
    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}